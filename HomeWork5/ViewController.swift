//
//  ViewController.swift
//  HomeWork5
//
//  Created by Nazar Starantsov on 21.10.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    lazy var imageView: UIImageView = {
        let image = UIImage(named: "dollarsign")
        let iv = UIImageView(image: image)
        iv.translatesAutoresizingMaskIntoConstraints = false
        
        return iv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupConstraints()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupAnimations()
    }
    func setupSubviews() {
        view.addSubview(imageView)
    }
    
    func setupConstraints() {
        imageView.widthAnchor.constraint(equalToConstant: 256).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 256).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func setupAnimations(){

        let animation1 = CABasicAnimation(keyPath: "opacity")
        animation1.fromValue = 0.0
        animation1.toValue = 1.0
        animation1.beginTime = 0.5
        animation1.duration = 3.0
        animation1.autoreverses = true

        let animation2 = CABasicAnimation(keyPath: "transform.scale")
        animation2.fromValue = 1
        animation2.toValue = 2
        animation2.beginTime = animation1.beginTime + animation1.duration
        animation2.duration = 3.0
        animation2.autoreverses = true
        
        let animation3 = CABasicAnimation(keyPath: "transform.scale")
        animation3.fromValue = 2
        animation3.toValue = 1
        animation3.beginTime = animation2.beginTime + animation2.duration
        animation3.duration = 1.0
        
        let animations  = CAAnimationGroup()
        animations.animations = [animation1, animation2, animation3]
        animations.duration = animation3.beginTime + animation3.duration
        
        imageView.layer.add(animations, forKey: "anims")
    }

}


